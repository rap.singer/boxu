const WIDTH = 375;//页面尺寸
const setView = () =>{
	let clientWidth = document.documentElement.clientWidth 
	if(clientWidth >= 1024){
		clientWidth = 1024
	}
	// 设置html的fontsize
	document.documentElement.style.fontSize = 
	(15.3* clientWidth) / WIDTH + 'px';
}
window.onresize = setView;
setView();